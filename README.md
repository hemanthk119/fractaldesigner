Fractal Designer
==============

Made with C# and WPF, Fractal Designer allows you to create fractals easily. With conditional and probabilistic activation, random and complicated fractals can be created.

 