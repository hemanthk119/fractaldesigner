﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FractalTest.ViewModels;
using System.Windows;
using System.Windows.Media;
using System.Xml.Serialization;

namespace FractalTest
{
    [Serializable]
    public class SaveTransform
    {
        public double SizeRatio { get; set; }
        public bool Flip { get; set; }
        public double BaseRotation { get; set; }
        public Color FillBrush { get; set; }
        public Color LineBrush { get; set; }
        public double Probability { get; set; }
        public bool EndHere { get; set; }

        public int StartLevel { get; set; }
        public int EndLevel { get; set; }

    }

    [Serializable]
    public class SavePoint
    {
        public List<SaveTransform> Transforms { get; set; }
        public Point Point { get; set; }
        public string SegementType { get; set; }
        public double ArcRadius { get; set; }
    }

    [Serializable]
    [XmlRoot("SaveFormat")]
    public class SaveFormat
    {
        public List<SavePoint> PointControls { get; set; }
        public Point ReferencePoint { get; set; }
        public Color CanvasColor { get; set; }
        public Color FillColor { get; set; }
        public Color LineColor { get; set; }
        public int Steps { get; set; }

        public static List<SaveTransform> SaveTransformsFromControl(PointControl pointControl)
        {
            List<SaveTransform> saveTransforms = new List<SaveTransform>();
            foreach(TransformProperty transform in pointControl.Transforms)
            {
                saveTransforms.Add(new SaveTransform() {
                     BaseRotation = transform.BaseRotation,
                     EndHere = transform.EndHere,
                     EndLevel = transform.EndLevel,
                     FillBrush = transform.FillBrush,
                     LineBrush = transform.LineBrush,
                     Flip = transform.Flip,
                     Probability = transform.Probability,
                     SizeRatio = transform.SizeRatio,
                     StartLevel = transform.StartLevel
                });
            }

            return saveTransforms;
        }

        public static List<SavePoint> SavePointFromControls(List<PointControl> pointControls)
        {
            List<SavePoint> savePoints = new List<SavePoint>();
            pointControls.ForEach(r =>
            {
                savePoints.Add(new SavePoint()
                {
                    ArcRadius = r.Size,
                    Point = r.Point,
                    SegementType = r.SegmentType,
                    Transforms = SaveTransformsFromControl(r)
                });
            });

            return savePoints;
        }
    }

    
}
