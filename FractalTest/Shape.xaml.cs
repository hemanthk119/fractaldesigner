﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FractalTest
{
    public partial class Shape : UserControl
    {
        private PointControl startPoint;
        public PointControl StartPoint {
            get { return startPoint; }
            set {
                if(startPoint != null)
                    startPoint.Move -= StartPoint_Move;
               
                startPoint = value;
                if (startPoint != null)
                {
                    pathFigure.StartPoint = startPoint.Point;
                    startPoint.Move += StartPoint_Move;
                }
                else
                    pathFigure.StartPoint = new Point();
            }
        }

        private void StartPoint_Move(object sender, EventArgs e)
        {
            pathFigure.StartPoint = (sender as PointControl).Point;
        }

        public Canvas Canvas { get; set; }

        private PathGeometry geop = new PathGeometry();
        public Path path { get; set; }
        public PathFigure pathFigure = new PathFigure();


        public List<PointControl> PointControls = new List<PointControl>();

        public Shape(Canvas canvas)
        {
            InitializeComponent();

            Canvas = canvas;

            path = new Path();


            //(DataContext as ViewModels.ShapeViewModel).FillColor = Colors.Transparent;
            //(DataContext as ViewModels.ShapeViewModel).LineColor = Colors.Black;

            path.Data = geop;
            path.Stroke = new SolidColorBrush(Colors.Black);
            path.StrokeThickness = 1;

            geop.Figures.Add(pathFigure);

            Canvas.Children.Add(path);
        }

        public void AddPoint(Point point, string type, double arcRadius = 400, List<TransformProperty> transforms = null)
        {
            PointControl pointControl = new PointControl(point, type) { Size = arcRadius, SegmentType=type, PathFigure = pathFigure, Canvas = Canvas, ShapeViewModel = this.DataContext as ViewModels.ShapeViewModel };
            Path selectedPath = (this.DataContext as ViewModels.ShapeViewModel).SelectedPath;


            pointControl.Move += (o, e) =>
            {
                Geometry geo = path.Data;
                selectedPath.Data = geo;
                selectedPath.InvalidateVisual();
            };

            pointControl.Change += (o, e) =>
            {
                Geometry geo = path.Data;
                selectedPath.Data = geo;
                selectedPath.InvalidateVisual();
            };

            pointControl.Removed += (o, e) =>
            {
                PointControls.Remove(pointControl);

                if (pointControl == StartPoint)
                {
                    StartPoint = PointControls.FirstOrDefault();
                }

                if(pointControl == (DataContext as ViewModels.ShapeViewModel).ReferencePoint)
                {
                    (DataContext as ViewModels.ShapeViewModel).ReferencePoint = PointControls.FirstOrDefault();
                }
               
            };

            if (PointControls.Count == 0)
            {
                StartPoint = pointControl;
                (DataContext as ViewModels.ShapeViewModel).ReferencePoint = pointControl;
            }

            if (transforms != null)
            {
                transforms.ForEach(r =>
                {
                    r.Parent = pointControl;
                    pointControl.Transforms.Add(r);
                });      
            }

            PointControls.Add(pointControl);

            pathFigure.Segments.Add((PathSegment)pointControl.PathSegment);
            Canvas.Children.Add(pointControl);

            selectedPath.Data = path.Data;
            selectedPath.InvalidateVisual();

        }


    }
}
