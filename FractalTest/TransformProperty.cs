﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Media;
using System.Windows;

namespace FractalTest
{
    public class TransformProperty : INotifyPropertyChanged
    {
        private double sizeRatio;
        private bool flip;
        private double baseRotation;
        private Color fillBrush;
        private Color lineBrush;
        private double probability;
        private bool endHere;
        
        private int startLevel;
        private int endLevel;

        public PointControl Parent { get; set; }
        public PointControl StartPoint { get; set; }

        public bool EndHere
        {
            get
            {
                return endHere;
            }

            set
            {
                endHere = value;
                OnPropertyChanged("EndHere");
            }
        }

        public double Probability
        {
            get
            {
                return probability;
            }

            set
            {
                probability = value;
                if (probability < 0)
                    probability = 0;
                else if (probability > 1)
                    probability = 1;
                OnPropertyChanged("Probability");
                if (Parent != null)
                    Parent.ShapeViewModel.SelectedTransform = this;
            }
        }

        public bool Flip
        {
            get
            {
                return flip;
            }

            set
            {
                flip = value;
                OnPropertyChanged("Flip");
                if (Parent != null)
                    Parent.ShapeViewModel.SelectedTransform = this;
            }
        }

        public int EndLevel
        {
            get
            {
                return endLevel;
            }

            set
            {
                if (value < 0)
                    endLevel = 0;
                else
                    endLevel = value;

                OnPropertyChanged("EndLevel");
            }
        }

        public int StartLevel
        {
            get
            {
                return startLevel;
            }

            set
            {
                if (value < 1)
                    startLevel = 1;
                else
                    startLevel = value;

                OnPropertyChanged("StartLevel");

            }
        }

        public double SizeRatio
        {
            get
            {
                return sizeRatio;
            }

            set
            {
                sizeRatio = value;
                OnPropertyChanged("SizeRatio");
                if(Parent != null)
                    Parent.ShapeViewModel.SelectedTransform = this;
            }
        }

        public double BaseRotation
        {
            get
            {
                return baseRotation;
            }

            set
            {
                baseRotation = value;
                OnPropertyChanged("BaseRotation");
                if (Parent != null)
                    Parent.ShapeViewModel.SelectedTransform = this;
            }
        }

        public Color FillBrush
        {
            get
            {
                return fillBrush;
            }

            set
            {
                fillBrush = value;
                OnPropertyChanged("FillBrush");
                if (Parent != null)
                    Parent.ShapeViewModel.SelectedTransform = this;
            }
        }

        public Color LineBrush
        {
            get
            {
                return lineBrush;
            }

            set
            {
                lineBrush = value;
                OnPropertyChanged("LineBrush");
                if (Parent != null)
                    Parent.ShapeViewModel.SelectedTransform = this;
            }
        }

        public TransformProperty(PointControl parent)
        {
            Parent = parent;

            SizeRatio = 0.5;
            BaseRotation = 0;
            StartLevel = 1;
            Probability = 1;
            FillBrush = Colors.Transparent;
            LineBrush = Colors.Black;
            EndLevel = 0;       
        }

        public TransformGroup ConvertToTransform(Point rotationCenter, Point StartPoint)
        {
            TransformGroup group = new TransformGroup();

            List<Transform> transforms = new List<Transform>();

            if (Flip)
                group.Children.Add(new ScaleTransform(-1, 1) { CenterX = StartPoint.X, CenterY = StartPoint.Y });

            group.Children.Add(new TranslateTransform() { X= rotationCenter.X - StartPoint.X, Y=rotationCenter.Y - StartPoint.Y  });
            group.Children.Add(new ScaleTransform( SizeRatio, SizeRatio) { CenterX = rotationCenter.X, CenterY = rotationCenter.Y });
            group.Children.Add(new RotateTransform(BaseRotation) { CenterX = rotationCenter.X, CenterY = rotationCenter.Y });



            return group;
        }

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
