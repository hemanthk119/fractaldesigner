﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using MahApps.Metro.Controls;
using System.Globalization;
using System.ComponentModel;
using System.Threading;
using System.Windows.Markup;
using Microsoft.Win32;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace FractalTest
{
    public class TransformConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            TransformProperty property = (value as TransformProperty);

            if (property != null)
                return property.ConvertToTransform(property.Parent.Point, MainWindow.Current.shapeViewModel.ReferencePoint.Point);
            else
                return new TransformGroup();
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new NotImplementedException();
        }
    }

    public class TransformFillConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            TransformProperty property = (value as TransformProperty);

            if (property != null)
                return new SolidColorBrush(property.FillBrush);
            else
                return new SolidColorBrush(Colors.Transparent);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new NotImplementedException();
        }
    }

    public class TransformStrokeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            TransformProperty property = (value as TransformProperty);

            if (property != null)
                return new SolidColorBrush(property.LineBrush);
            else
                return new SolidColorBrush(Colors.Transparent);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new NotImplementedException();
        }
    }

    public class VisibilityLineTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((string)value == "Line")
                return Visibility.Hidden;
            else
                return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ColorToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new SolidColorBrush((Color)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public partial class MainWindow : MetroWindow, INotifyPropertyChanged
    {
        public Shape shape;

        private ViewModels.ShapeViewModel shapeVM;
        public ViewModels.ShapeViewModel shapeViewModel {
            get
            {
                return shapeVM;
            }
            set
            {
                shapeVM = value;
                OnPropertyChanged("shapeViewModel");
            }
        }

        public static MainWindow Current;

        public static DependencyProperty CanvasColorProperty = DependencyProperty.Register("CanvasColor", typeof(Color), typeof(MainWindow));
        public Color CanvasColor
        {
            get
            {
                return (Color)this.GetValue(CanvasColorProperty);
            }

            set
            {
                this.SetValue(CanvasColorProperty, value);
            }
        }

        public static DependencyProperty StepsProperty = DependencyProperty.Register("Steps", typeof(int), typeof(MainWindow));

        public int Steps
        {
            get
            {
                return (int)this.GetValue(StepsProperty);
            }

            set
            {
                this.SetValue(StepsProperty, value);
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            CanvasColor = Colors.White;
            Steps = 6;

            shapeViewModel = new ViewModels.ShapeViewModel();
            shapeViewModel.SelectedPath = selectedPath;

            shape = new Shape(canvas) { DataContext = shapeViewModel };

            shapeViewModel.Path = shape.path;
            shapeViewModel.FillColor = Colors.Transparent;
            shapeViewModel.LineColor = Colors.Black;

            canvas.MouseLeftButtonDown += (o, e) =>
            {
                if (e.ClickCount == 2)
                {
                    Point p = e.GetPosition(this);
                    p.Offset(0, -30);
                    shape.AddPoint(p, shapeViewModel.SelectedLineType, shapeViewModel.SelectedArcRadius);
                }
            };

            Current = this;
            DataContext = this;

            ControlsStack.Children.Add(shape);
        }

        private void Th_DragDelta(object sender, DragDeltaEventArgs e)
        {
            UIElement thumb = sender as UIElement;
            Canvas.SetLeft(thumb, Canvas.GetLeft(thumb) + e.HorizontalChange);
            Canvas.SetTop(thumb, Canvas.GetTop(thumb) + e.VerticalChange);
        }

        private void Button_Run_Click(object sender, RoutedEventArgs e)
        {
            OutputWindow window = new OutputWindow(shapeViewModel.Path, shape.PointControls, shapeViewModel.ReferencePoint, new SolidColorBrush(CanvasColor), new SolidColorBrush(shapeViewModel.FillColor), new SolidColorBrush(shapeViewModel.LineColor), Steps);
            window.ShowDialog();
        }

        private void Button_File_Menu_Click(object sender, RoutedEventArgs e)
        {
            (sender as Button).ContextMenu.IsOpen = true;
        }

        private void SaveFractalFile()
        {
            if (shape.PointControls.Count > 0)
            {
                SaveFormat format = new SaveFormat();

                format.CanvasColor = CanvasColor;
                format.FillColor = shapeViewModel.FillColor;
                format.LineColor = shapeViewModel.LineColor;
                format.PointControls = SaveFormat.SavePointFromControls(shape.PointControls);
                format.ReferencePoint = shapeViewModel.ReferencePoint.Point;
                format.Steps = Steps;

                SaveFileDialog save = new SaveFileDialog();
                save.AddExtension = true;
                save.DefaultExt = ".frac";
                save.Filter = "Fractal Files | *.frac";

                save.FileOk += (o, e) =>
                {
                    var stream = File.Open(save.FileName, FileMode.Create);

                    XmlSerializer serializer = new XmlSerializer(typeof(SaveFormat));
                    serializer.Serialize(stream, format);

                    stream.Close();     
                };

                save.ShowDialog();
            }
        }

        private void OpenFractalFile()
        {
            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "Fractal Files | *.frac";
            file.DefaultExt = "*.frac";
            file.FileOk += (o, e) =>
            {
                try
                {
                    ReadFractalFile(file.FileName);
                }

                catch
                {
                    System.Windows.MessageBox.Show("File Read Error.");
                }
            };
            file.ShowDialog();
        }

        private void ReadFractalFile(string path)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SaveFormat));
            SaveFormat format = (SaveFormat)serializer.Deserialize(File.Open(path, FileMode.Open));


            List<UIElement> removethese = new List<UIElement>();
            for (int i = 0; i < canvas.Children.Count; i++)
            {
                if (canvas.Children[i] != selectedPath)
                    removethese.Add(canvas.Children[i]);
            }
            removethese.ForEach(r =>
            {
                canvas.Children.Remove(r);
            });
            removethese.Clear();
            ControlsStack.Children.Remove(shape);
            shapeViewModel = null;
            shape = null;
            Steps = 0;
            CanvasColor = Colors.Transparent;


            Steps = format.Steps;
            CanvasColor = format.CanvasColor;
            shapeViewModel = new ViewModels.ShapeViewModel();
            shapeViewModel.SelectedPath = selectedPath;


            shape = new Shape(canvas) { DataContext = shapeViewModel };
            shapeViewModel.Path = shape.path;

            ControlsStack.Children.Add(shape);

            shapeViewModel.FillColor = format.FillColor;
            shapeViewModel.LineColor = format.LineColor;

            foreach (SavePoint point in format.PointControls)
            {
                List<TransformProperty> transforms = new List<TransformProperty>();
                foreach(SaveTransform transform in point.Transforms)
                {
                    transforms.Add(new TransformProperty(null) {
                         BaseRotation = transform.BaseRotation,
                         EndHere = transform.EndHere,
                         EndLevel = transform.EndLevel,
                         FillBrush = transform.FillBrush,
                         Flip = transform.Flip,
                         LineBrush = transform.LineBrush,
                         Probability = transform.Probability,
                         SizeRatio = transform.SizeRatio,
                         StartLevel = transform.StartLevel
                    });
                }

                shape.AddPoint(point.Point, point.SegementType, point.ArcRadius, transforms);
            }
        }

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Menu_Open_Click(object sender, RoutedEventArgs e)
        {
            OpenFractalFile();
        }

        private void Menu_Save_Click(object sender, RoutedEventArgs e)
        {
            SaveFractalFile();
        }
    }
}
