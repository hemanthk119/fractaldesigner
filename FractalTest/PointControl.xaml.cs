﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FractalTest
{
    /// <summary>
    /// Interaction logic for PointControl.xaml
    /// </summary>
    /// 

    public class RotationVisibilityTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((string)value)
            {
                case "Arc":
                    return System.Windows.Visibility.Visible;
                default:
                    return System.Windows.Visibility.Hidden;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new NotImplementedException();
        }
    }

    public class RefrencePointCurrentConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] == values[1])
                return false;

            return true;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class SizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double val = (double)value;
            val = Math.Abs(val);
            return new Size((double)val, (double)val);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new NotImplementedException();
        }
    }

    public class SweepSizeConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((double)value < 0)
                return SweepDirection.Clockwise;
            else
                return SweepDirection.Counterclockwise;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new NotImplementedException();
        }
    }


    public partial class PointControl : UserControl
    {
        public PathFigure PathFigure { get; set; }
        public Canvas Canvas { get; set; }
        public ViewModels.ShapeViewModel ShapeViewModel { get; set; }

        public ObservableCollection<TransformProperty> Transforms { get; set; }

        public static readonly DependencyProperty PointProperty = DependencyProperty.Register("Point", typeof(Point), typeof(PointControl));
      
        public Point Point
        {
            get
            {
                return (Point)this.GetValue(PointProperty);
            }

            set
            {
                this.SetValue(PointProperty, value);
            }
        }

        public delegate void RemovedEventHandler(object sender, EventArgs e);
        public event RemovedEventHandler Removed;

        protected virtual void OnRemoved(object sender, EventArgs e)
        {
            if (Removed != null)
                Removed(sender, e);
        }

        public delegate void MoveEventHandler(object sender, EventArgs e);
        public event MoveEventHandler Move;

        protected virtual void OnMove(object sender, EventArgs e)
        {
            if (Move != null)
                Move(sender, e);
        }

        public delegate void ChangeEventHandler(object sender, EventArgs e);
        public event ChangeEventHandler Change;

        protected virtual void OnChange(object sender, EventArgs e)
        {
            if (Change != null)
                Change(sender, e);
        }



        public static readonly DependencyProperty SizeProperty = DependencyProperty.Register("Size", typeof(double), typeof(PointControl));

        public double Size
        {
            get
            {
                return (double)this.GetValue(SizeProperty);
            }

            set
            {
                this.SetValue(SizeProperty, value);
                OnChange(this, new EventArgs());
                MainWindow.Current.shapeViewModel.SelectedArcRadius = value;
            }
        }

        public static readonly DependencyProperty SegmentedProperty = DependencyProperty.Register("SegmentType", typeof(string), typeof(PointControl));

        public static readonly DependencyProperty PathSagmentProperty = DependencyProperty.Register("PathSegment", typeof(PathSegment), typeof(PointControl));

        public PathSegment PathSegment
        {
            get
            {
                var type = (string)this.GetValue(SegmentedProperty);
                switch(type)
                {
                    case "Arc":
                        return (PathSegment)this.GetValue(PathSagmentProperty);
                    case "Line":
                    default:
                        return (PathSegment)this.GetValue(PathSagmentProperty);
                }
                
            }
        }

        public string SegmentType
        {
            get
            {
                return (string)this.GetValue(SegmentedProperty);
            }

            set
            {
                switch((string)value)
                {
                    case "Arc":
                        ArcSegment arc;
                        if (PathSegment == null)
                        {
                            arc = new ArcSegment();
                            arc.SweepDirection = SweepDirection.Clockwise;
                        }
                        else
                        {
                            PathSegment path = (PathSegment)PathSegment;

                            if (path.GetType() != typeof(ArcSegment))
                            {
                                var point = (path as LineSegment).Point;

                                var index = PathFigure.Segments.IndexOf(path);
                                Size = ShapeViewModel.SelectedArcRadius;
                                path = new ArcSegment() { Point = point, SweepDirection = Size > 0 ? SweepDirection.Clockwise : SweepDirection.Counterclockwise, Size = new System.Windows.Size(Math.Abs(Size), Math.Abs(Size)) };
                                PathFigure.Segments[index] = path;
                            }

                            arc = (ArcSegment)path;
                        }

                        BindingBase sourceBinding = new Binding { Source = this, Path = new PropertyPath(PointControl.PointProperty) };
                        BindingOperations.SetBinding(arc, ArcSegment.PointProperty, sourceBinding);

                        sourceBinding = new Binding { Source = this, Converter = new SizeConverter(), ConverterParameter = new { PointControls = MainWindow.Current.shape.PointControls, Self=this } , Path = new PropertyPath(PointControl.SizeProperty) };
                        BindingOperations.SetBinding(arc, ArcSegment.SizeProperty, sourceBinding);

                        sourceBinding = new Binding { Source = this, Converter = new SweepSizeConverter(), ConverterParameter = new { PointControls = MainWindow.Current.shape.PointControls, Self = this }, Path = new PropertyPath(PointControl.SizeProperty) };
                        BindingOperations.SetBinding(arc, ArcSegment.SweepDirectionProperty, sourceBinding);

                        this.SetValue(PathSagmentProperty, arc);


                        this.SetValue(SegmentedProperty, (string)value);
                        
                        break;
                    case "Line":
                    default:

                        LineSegment line;
                        if (PathSegment == null)
                            line = new LineSegment();
                        else
                        {
                            PathSegment path = (PathSegment)PathSegment;
                           
                            if(path.GetType() != typeof(LineSegment))
                            {
                                var point = (path as ArcSegment).Point;
                                var index = PathFigure.Segments.IndexOf(path);
                                path = new LineSegment() { Point = point };
                                PathFigure.Segments[index] = path;
                            }
                            line = (LineSegment)path;
                        }

                        line.Point = Point;

                        BindingBase sourceBinding2 = new Binding { Source = this, Path = new PropertyPath(PointControl.PointProperty) };
                        BindingOperations.SetBinding(line, LineSegment.PointProperty, sourceBinding2);

                        this.SetValue(SegmentedProperty, (string)value);
                        this.SetValue(PathSagmentProperty, line);

                        break;
                }

                OnChange(this, new EventArgs());

            }
        }

        private RelayCommand deleteCommand;
        public RelayCommand DeleteCommand
        {
            get
            {
                return deleteCommand;
            }

            set
            {
                deleteCommand = value;
            }
        }
        public void deleteCommandMethod(object item)
        {
            TransformProperty trans = (item as TransformProperty);
            Transforms.Remove(trans);
        }

        public PointControl(Point point, string type)
        {
            InitializeComponent();
            Point = point;
            Canvas.SetTop(this, point.Y - 5);
            Canvas.SetLeft(this, point.X - 5);

            SegmentType = type;

            Transforms = new ObservableCollection<TransformProperty>();

            DeleteCommand = new RelayCommand(new Action<object>(deleteCommandMethod));

            DataContext = this;
        }

        private void Thumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            Canvas.SetLeft(this, Canvas.GetLeft(this) + e.HorizontalChange);
            Canvas.SetTop(this, Canvas.GetTop(this) + e.VerticalChange);
            Point = new Point(Canvas.GetLeft(this) + 5, Canvas.GetTop(this) + 5);

            OnMove(this, new EventArgs());
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SegmentType = ((sender as ComboBox).SelectedItem as ComboBoxItem).Content as string;
            MainWindow.Current.shapeViewModel.SelectedLineType = SegmentType;
        }

        private void Thumb_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            Size += e.Delta * 0.1;
        }

        private void Remove_Button_Click(object sender, RoutedEventArgs e)
        {
            PathFigure.Segments.Remove(PathSegment);
            Canvas.Children.Remove(this);

            OnRemoved(this, new EventArgs());

            PointMenu.IsOpen = false;
        }

        private void TextBox_Rotation_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                try
                {
                    var Size = double.Parse((sender as TextBox).Text);
                }
                catch
                {
                    (sender as TextBox).Text = "100";
                    Size = 100;
                }
            }
        }

        private void TransformAdd_Click(object sender, RoutedEventArgs e)
        {
            Transforms.Add(new TransformProperty(this));
        }

        private void ListBox_Transforms_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MainWindow.Current.shapeViewModel.SelectedTransform = (TransformProperty)(sender as ListBox).SelectedItem;
        }

        private void ContextMenu_Closed(object sender, RoutedEventArgs e)
        {
            MainWindow.Current.shapeViewModel.SelectedTransform = null;
        }

        private void SetRef_Click(object sender, RoutedEventArgs e)
        {
            ShapeViewModel.ReferencePoint = this;
            PointMenu.IsOpen = false;
        }

        private void TextBox_Transofrm_Size_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            string currentVal = (sender as TextBox).Text;
            try
            {
                double val = double.Parse(currentVal);
                val += e.Delta * 0.0001;

                if (val <= 0)
                    val = 0;

                (sender as TextBox).Text = val.ToString();
            }
            catch
            {
                (sender as TextBox).Text = "0.5";
            }
            e.Handled = true;

        }

        private void TextBox_Transofrm_Rotate_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            string currentVal = (sender as TextBox).Text;
            try
            {
                double val = double.Parse(currentVal);
                val += e.Delta * 0.01;

                (sender as TextBox).Text = val.ToString();

            }
            catch
            {
                (sender as TextBox).Text = "0";
            }

            e.Handled = true;
        }

        private void TextBox_Rotation_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            Size += e.Delta * 0.1;
            e.Handled = true;
        }
    }
}
