﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Shapes;


namespace FractalTest.ViewModels
{
    public class ShapeViewModel : INotifyPropertyChanged
    {
        private string selectedLineType;
        public string SelectedLineType
        {
            get
            {
                return selectedLineType;
            }

            set
            {
                selectedLineType = value;
                OnPropertyChanged("SelectedLineType");
            }
        }

        private double selectedArcRadius;
        public double SelectedArcRadius
        {
            get
            {
                return selectedArcRadius;
            }

            set
            {
                selectedArcRadius = value;
                OnPropertyChanged("SelectedArcRadius");
            }
        }

        private Color fillColor;
        public Color FillColor
        {
            get
            {
                return fillColor;
            }

            set
            {
                fillColor = value;
                Path.Fill = new SolidColorBrush(fillColor);
                Path.InvalidateVisual();
                OnPropertyChanged("FillColor");
            }
        }

        private Color lineColor;
        public Color LineColor
        {
            get
            {
                return lineColor;
            }

            set
            {
                lineColor = value;
                Path.Stroke = new SolidColorBrush(value);
                Path.InvalidateVisual();
                OnPropertyChanged("LineColor");
            }
        }

        public Path Path { get; set; }

        private PointControl referencePoint;
        public PointControl ReferencePoint
        {
            get { return referencePoint; }
            set
            {
                referencePoint = value;
                OnPropertyChanged("ReferencePoint");
            }
        }

        private TransformProperty selectedTransfrom;
        public TransformProperty SelectedTransform
        {
            get
            {
                return selectedTransfrom;
            }

            set
            {
                selectedTransfrom = value;
                OnPropertyChanged("SelectedTransform");
            }
        }

        private Path selectedPath;
        public Path SelectedPath
        {
            get
            {
                return selectedPath;
            }

            set
            {
                selectedPath= value;
                OnPropertyChanged("SelectedPath");
            }
        }

        public ShapeViewModel()
        {
            SelectedLineType = "Line";
            SelectedArcRadius = 400;
        }


        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
