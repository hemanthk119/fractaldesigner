﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.ComponentModel;
using System.Windows.Threading;
using Microsoft.Win32;

namespace FractalTest
{
    public partial class OutputWindow : MetroWindow
    {

        public Path Path { get; set; }
        public List<PointControl> PointControls { get; set; }
        public PointControl ReferencePoint { get; set; }

        private Random Random = new Random();

        private int MaxSteps = 0;
       
        public OutputWindow(Path path, List<PointControl> pointControls, PointControl referencePoint, Brush canvasColor, Brush fillColor, Brush lineColor, int steps)
        {
            InitializeComponent();
            Path = new Path() {Data = path.Data };
            PointControls = pointControls;
            ReferencePoint = referencePoint;

            Path.Fill = fillColor;
            Path.Stroke = lineColor;

            canvas.Children.Add(Path);

            canvas.Background = canvasColor;

            MaxSteps = steps;

            foreach (PointControl point in pointControls)
            {
                GeneratePath(1, point, referencePoint);
            }
        }

        public void GeneratePath(int step, PointControl currentPoint, PointControl originalReferencePoint, double prevRotation = 0, double prevSize = 1, TransformGroup prevTransform = null)
        {
            foreach (TransformProperty transform in currentPoint.Transforms)
            {
                // Check if tranformation is active on this level and check probablity of activation
                if ((transform.StartLevel <= step && (transform.EndLevel >= step || transform.EndLevel == 0)) && transform.Probability >= Random.NextDouble())
                {
                    //Create copy of the path
                    Path currentPath = new Path() { Data = Path.Data };
                    Point originalRefPoint = originalReferencePoint.Point;

                    currentPath.Fill = new SolidColorBrush(transform.FillBrush);
                    currentPath.Stroke = new SolidColorBrush(transform.LineBrush);

                    //Set size and rotation, depending on the previous size and rotation
                    double sizeRatio = prevSize * transform.SizeRatio;
                    double rotation = prevRotation + transform.BaseRotation;

                    //Create transform objects and add to group
                    TransformGroup currentTransform = new TransformGroup();

                    if (transform.Flip)
                        currentTransform.Children.Add(new ScaleTransform(-1, 1) { CenterX = originalRefPoint.X, CenterY = originalRefPoint.Y });

                    currentTransform.Children.Add(new ScaleTransform() { ScaleX = sizeRatio, ScaleY = sizeRatio, CenterX = originalRefPoint.X, CenterY = originalRefPoint.Y });
                    currentTransform.Children.Add(new RotateTransform() { Angle = rotation, CenterX = originalRefPoint.X, CenterY = originalRefPoint.Y });

                    if (prevTransform == null)
                    {
                        currentTransform.Children.Add(new TranslateTransform() { X = currentPoint.Point.X - originalRefPoint.X, Y = currentPoint.Point.Y - originalRefPoint.Y });
                    }
                    else
                    {
                        Point currentPointTransformed = prevTransform.Transform(currentPoint.Point);
                        currentTransform.Children.Add(new TranslateTransform() { X = currentPointTransformed.X - originalRefPoint.X, Y = currentPointTransformed.Y - originalRefPoint.Y });
                    }

                    //Apply transform to path
                    currentPath.RenderTransform = currentTransform;

                    //Add path to canvas
                    canvas.Children.Add(currentPath);

                    //Go to the next level and repeat
                    if (step < MaxSteps && !transform.EndHere)
                        foreach (PointControl point in PointControls)
                            GeneratePath(step + 1, point, originalReferencePoint, rotation, sizeRatio, currentTransform);              
                }
            }
        }

        private void Menu_Save_PNG_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.DefaultExt = "*.png";
            save.Filter = "PNG Image | *.png";
            save.FileOk += (o, ev) =>
            {
                RenderTargetBitmap rtb = new RenderTargetBitmap((int)canvas.RenderSize.Width,
                (int)canvas.RenderSize.Height, 96d, 96d, System.Windows.Media.PixelFormats.Default);
                rtb.Render(canvas);

                BitmapEncoder pngEncoder = new PngBitmapEncoder();
                pngEncoder.Frames.Add(BitmapFrame.Create(rtb));

                using (var fs = System.IO.File.Create(save.FileName))
                {
                    pngEncoder.Save(fs);
                }
            };
            save.ShowDialog();
        }

    }
}
